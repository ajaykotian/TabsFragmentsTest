package com.example.ajaykotian.test;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.os.AsyncTaskCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {

    protected EditText etUrl;
    protected ImageButton downloadButton;
    protected ImageView ivResult;

    protected WebView webView;


    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blank, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        downloadButton = (ImageButton) view.findViewById(R.id.ib_load);
        webView = (WebView) view.findViewById(R.id.webview);
        ivResult = (ImageView) view.findViewById(R.id.iv_result);

        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String urlString = /*etUrl.getText().toString()*/ "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/2000px-Google_2015_logo.svg.png";
                new ImageLoad().execute(urlString);
               //webView.loadUrl("http://www.google.com");
            }
        });

    }

    protected class ImageLoad extends AsyncTask<String,Void,Bitmap>{


        @Override
        protected void onPreExecute() {

            downloadButton.setVisibility(View.GONE);
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            URL url = null;
            Bitmap bmp = null;
            try {
                url = new URL(params[0]);
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return bmp;
        }

        @Override
        protected void onPostExecute(Bitmap bmp) {

            if(bmp!=null)
            {
                ivResult.setImageBitmap(bmp);
            }

            downloadButton.setVisibility(View.VISIBLE);
        }
    }
}
