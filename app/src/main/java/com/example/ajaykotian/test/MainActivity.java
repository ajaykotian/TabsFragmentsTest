package com.example.ajaykotian.test;

import android.animation.Animator;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.example.ajaykotian.test.utils.SlidingTabLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    protected SlidingTabLayout slidingTabLayout;
    protected ViewPager viewPager;
    protected FloatingActionButton fab_add,fab_del;

    protected FragmentAdapter fragmentAdapter;

    protected List<String> titles;
    protected List<Fragment> fragments;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setElevation(0);

        titles = new ArrayList<String>();
        fragments = new ArrayList<Fragment>();
        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        initTabs();

        fab_add = (FloatingActionButton) findViewById(R.id.fab_add);
        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add();
            }
        });

        fab_del = (FloatingActionButton) findViewById(R.id.fab_del);
        fab_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               delete();
            }
        });
    }

    public void initTabs()
    {
        fragmentAdapter = new FragmentAdapter(getSupportFragmentManager(),fragments,titles);

        viewPager.setAdapter(fragmentAdapter);

        slidingTabLayout.setDistributeEvenly(false);

        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white);
            }
        });

        slidingTabLayout.setViewPager(viewPager);
    }

    public void add() {

        titles.add((titles.size()+1)+"");
        fragments.add(new BlankFragment());

        initTabs();
    }

    public void delete()
    {
        if(titles.size()>0) {
            titles.remove(titles.size() - 1);
            fragments.remove(fragments.size() - 1);

            initTabs();
        }
        else {
            Toast.makeText(this,"Empty",Toast.LENGTH_SHORT).show();
        }
    }

    class FragmentAdapter extends FragmentStatePagerAdapter {

        private Fragment[] fragments;
        private int numPages;
        private String[] titles;

        public FragmentAdapter(FragmentManager fm, List<Fragment> fragments, List<String> titles) {
            super(fm);
            this.fragments = fragments.toArray(new Fragment[0]);
            this.numPages = fragments.size();
            this.titles =  titles.toArray(new String[0]);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments[position];
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return numPages;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
    }
}
